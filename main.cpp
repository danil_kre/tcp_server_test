#include "server/server.h"
#include "cmdline_parser/parser.h"

#include <thread>
#include <boost/program_options.hpp>

#include <iostream>

using namespace std;
using namespace boost::program_options;

int main(int argc, char** argv)
{
    int port = 0;
    CmdLineParser parser;
    parser.Parse(argc, argv);
    port = parser.GetPort();
    if (port <= 0)
    {
        std::cerr << "Port not specified correctly. Please launch program with 'port' option:"
                  << endl
                  << "./tcp_server --port <Number>" << endl;
        return 0;

    }
    Server::ServerPtr serverPtr = CreateServer(port);
    if (!serverPtr->Run())
        cerr << serverPtr->GetErrorText();

    return 0;
}

