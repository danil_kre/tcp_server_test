#pragma once

#include "hasher.h"

class StringHasher: public Hasher
{
public:
    StringHasher();
    void AddData(const char* data) override;
    std::string GetHash() const override;
    void Clear() override;
private:
    size_t Hash = 0;
};
