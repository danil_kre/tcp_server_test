#include "server_impl.h"
#include "hasher_impl.h"
#include "socket_wrapper.h"

#include <arpa/inet.h>
#include <sys/socket.h>

#include <thread>
#include <algorithm>
#include <unistd.h>


ServerImpl::ServerImpl(int port)
{
    Port = port;
}

ServerImpl::~ServerImpl()
{
}

std::string ServerImpl::GetErrorText() const
{
    return TextError;
}

void start_connection(SocketWrapper socket, std::unique_ptr<Hasher> hasher)
{
    char buf[1000];
    while (true)
    {
        int real_b = socket.Read(buf, sizeof(buf)-1);
        if (real_b <= 0)
            break;
        buf[real_b]='\0';
        char* cursor = buf;
        while (*cursor)
        {
            char* data = std::find(cursor, buf + real_b, '\n');
            if (data != buf + real_b)
            {
                *data='\0';
                hasher->AddData(cursor);
                std::string hash = hasher->GetHash();
                hasher->Clear();
                socket.Write(hash.c_str(), hash.length());
                cursor = data + 1;
            } else {
                hasher->AddData(cursor);
                break;
            }
        }
    }
}


bool ServerImpl::Run()
{
    bool ret = true;
    try
    {
        ListenSocket.reset(new SocketWrapper);
    }
    catch (std::runtime_error &error)
    {
        TextError = error.what() + std::string("\n");
        return false;
    }

    if (ListenSocket->Bind(Port) < 0)
    {
        TextError = "Can't bind socket on port " + std::to_string(Port) + "\n";
        return false;
    }

    if (ListenSocket->Listen() < 0)
    {
        TextError = "Can't listen socket\n";
        return false;
    }

    std::vector<std::thread> threads;
    while (true)
    {
        auto clientSocket = ListenSocket->Accept();
        if (clientSocket.IsBad())
        {
            break;
            ret = false;
        }
        threads.emplace_back(start_connection, std::move(clientSocket), std::unique_ptr<Hasher>(new StringHasher));
    }
    for(auto& thread: threads)
        thread.join();

    return ret;
}
