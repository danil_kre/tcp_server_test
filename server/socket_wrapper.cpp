#include "socket_wrapper.h"

#include <arpa/inet.h>
#include <sys/socket.h>

#include <unistd.h>
#include <stdexcept>

SocketWrapper::SocketWrapper()
{
    if ((Socket = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        throw std::runtime_error("Can't create socket");
    }
}

SocketWrapper::SocketWrapper(int socket)
{
    Socket = socket;
}

SocketWrapper::SocketWrapper(SocketWrapper&& other)
{
    Socket = other.Socket;
    other.Socket = 0;
}

SocketWrapper::~SocketWrapper()
{
    if (Socket)
        close(Socket);
    Socket = 0;
}

int SocketWrapper::Bind(int port)
{
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    return bind(Socket, (struct sockaddr*)&addr, sizeof(addr));
}

int SocketWrapper::Listen()
{
    return listen(Socket, 1);
}

SocketWrapper SocketWrapper::Accept()
{
    return accept(Socket, NULL, NULL);
}

bool SocketWrapper::IsBad() const
{
    return Socket <= 0;
}

int SocketWrapper::Read(void* buf, int bufSize)
{
    return read(Socket, buf, bufSize);
}

int SocketWrapper::Write(const char* buf, int bufSize)
{
    return write(Socket, buf, bufSize);
}
