#include "hasher_impl.h"

#include <iostream>
#include <sstream>
#include <string>

StringHasher::StringHasher()
{
    Clear();
}

void StringHasher::AddData(const char* data)
{
    size_t hash2 = std::hash<std::string>()(data);
    Hash = Hash ^ (hash2 << 1);
}

std::string StringHasher::GetHash() const
{
    std::stringstream stream;
    stream << std::hex << Hash << std::endl;
    return std::string(stream.str());
}

void StringHasher::Clear()
{
    Hash = std::hash<std::string>()(std::string());
}
