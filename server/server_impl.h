#pragma once

#include "server.h"

class SocketWrapper;

class ServerImpl: public Server
{
public:
    ServerImpl(int port);
    ~ServerImpl();

    bool Run() override;
    std::string GetErrorText() const override;

private:
    int Port = 54000;
    std::unique_ptr<SocketWrapper> ListenSocket;
    std::string TextError;
};
