#pragma once

class SocketWrapper final
{
public:
    SocketWrapper();
    SocketWrapper(int socket);

    SocketWrapper(const SocketWrapper&) = delete;
    SocketWrapper(SocketWrapper&&);
    ~SocketWrapper();

    int Bind(int port);
    int Listen();
    SocketWrapper Accept();
    int Read(void *buf, int bufSize);
    int Write(const char* buf, int bufSize);

    bool IsBad() const;

private:
    int Socket = 0;
};
