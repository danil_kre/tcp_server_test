#include "parser.h"

#include <boost/program_options.hpp>

#include <iostream>

using namespace boost::program_options;

std::string CmdLineParser::Parse(int argc, char **argv)
{
    try
    {
        options_description desc{"ServerOptions"};
        desc.add_options()
                ("help,h", "Help screen")
                ("port", value<int>()->default_value(0), "Port");
        variables_map argMap;
        store(parse_command_line(argc, argv, desc), argMap);
        notify(argMap);
        if (argMap.count("help"))
        {
            std::cout << "help, h  - show this screen" << std::endl
                      << "port <N> - TCP port number " << std::endl;
        } else if (argMap.count("port"))
        {
            Port = argMap["port"].as<int>();
        }
    }
    catch (const error &ex)
    {
        return ex.what();
    }
    return "";
}

int CmdLineParser::GetPort() const
{
    return Port;
}
