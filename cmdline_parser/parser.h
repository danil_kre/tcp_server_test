#include <string>

class CmdLineParser final
{
public:
    CmdLineParser() = default;

    std::string Parse(int argc, char** argv);
    int GetPort() const;
private:
    int Port = 0;
};
