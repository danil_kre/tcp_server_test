#include "cmdline_parser/parser.h"

#include <string>
#include <gtest/gtest.h>

TEST(ArgsTest, EmptyArgs)
{
    CmdLineParser parser;
    ASSERT_NO_THROW(parser.Parse(0, nullptr));
    ASSERT_TRUE(parser.GetPort() == 0);
}

TEST(ArgsTest, LackOfArgs)
{
    CmdLineParser parser;
    std::vector<char*> argv = {(char *)"./tcp_server"};
    ASSERT_NO_THROW(parser.Parse(argv.size(), argv.data()));
    ASSERT_TRUE(parser.GetPort() == 0);
}

TEST(ArgsTest, WrongArg)
{
    CmdLineParser parser;
    std::vector<char*> argv = {(char *)"./tcp_server", (char *)"--SomeWrongArgument"};
    std::string error;
    ASSERT_NO_THROW(error = parser.Parse(argv.size(), argv.data()));
    ASSERT_TRUE(parser.GetPort() == 0);
    ASSERT_TRUE(error.length() > 0);
}

TEST(ArgsTest, PortArg)
{
    CmdLineParser parser;
    std::vector<char*> argv = {(char *)"./tcp_server", (char *)"--port", (char *)"12345"};
    ASSERT_NO_THROW(parser.Parse(argv.size(), argv.data()));
    ASSERT_TRUE(parser.GetPort() == 12345);
}
