#include "server/socket_wrapper.h"

#include <gtest/gtest.h>

TEST(SocketTest, SocketCreation)
{
    SocketWrapper socket;
    ASSERT_FALSE(socket.IsBad());
}

TEST(SocketTest, SocketClosing)
{
    SocketWrapper socket;
    ASSERT_TRUE(socket.Bind(123000) == 0);
    ASSERT_FALSE(socket.IsBad());
    socket.~SocketWrapper();
    ASSERT_TRUE(socket.IsBad());
}

TEST(SocketTest, SocketMoving)
{
    SocketWrapper socket;
    ASSERT_TRUE(socket.Bind(123001) == 0);
    SocketWrapper anotherSocket(std::move(socket));
    ASSERT_TRUE(socket.IsBad());
    ASSERT_FALSE(anotherSocket.IsBad());
}
