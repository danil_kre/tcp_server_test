#include "server/server.h"
#include "server/hasher_impl.h"

#include <gtest/gtest.h>

TEST(ServerTest, ServerCreation)
{
    Server::ServerPtr server;
    ASSERT_NO_THROW(server = CreateServer(0));
    ASSERT_TRUE(server.get() != nullptr);
}

TEST(ServerTest, ServerEmptyError)
{
    Server::ServerPtr server;
    ASSERT_NO_THROW(server = CreateServer(0));
    ASSERT_TRUE(server->GetErrorText().empty());
}

TEST(HasherTest, HasherStabilityTest)
{
    StringHasher hasher1;
    hasher1.AddData("string1");
    StringHasher hasher2;
    hasher2.AddData("string1");
    ASSERT_TRUE(hasher1.GetHash() == hasher2.GetHash());
}

TEST(HasherTest, EmptyHasherTest)
{
    StringHasher hasher;
    ASSERT_TRUE(hasher.GetHash() != "");
}

TEST(HasherTest, ClearingHasherTest)
{
    StringHasher hasher;
    auto emptyHash = hasher.GetHash();
    hasher.AddData("some data");
    hasher.Clear();
    ASSERT_TRUE(emptyHash == hasher.GetHash());
}
